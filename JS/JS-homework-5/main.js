let tabs = document.querySelectorAll(".tabs-title");
let tabsText = document.querySelectorAll(".tabs-text")

for(let i =0;i<tabs.length;i++){
    tabs[i].addEventListener('click',()=>{
        for(let j=0;j<tabs.length;j++){
            tabs[j].classList.remove("active");
            tabsText[j].style.display = 'none';
        }
        tabs[i].classList.add('active');
        tabsText[i].style.display = 'block';
    })
}

