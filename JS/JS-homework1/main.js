/*
  ES6 let us that: We can create variable with "let" and "const". Because, we can meet problems with using "var" at some moments  

So, firstly we should know about variable declaration and initialization, scope, hoisting:
1.  var declaration; 
    declaration = 'It is initialization'; // in var when we declare variable it returns us undefined , we also can declare one variable many times with var:EX:
            var i=5;
            var i=8;

    //but in 'let' these causes to Reference Error (If we don't give value to variable or try to declare previous variable many times)

2.  Scope(block scope, functional scope, global scope):
block scope -> Every block such as loop(for, while, do{}while), conditions(switch case or if) and etc..In other words, this {} 
functional scope -> everywhere inside of function(){}
global scope -> place where every code is executed, or is visible anywhere in all other scopes
    **var is functional scope, so inside of function we can access any variable which is declared with var.EX:
            
            var i=25;
            function testingVar(){
                for(var i=0;i<5;i++){
                    console.log(i); // 0,1,2,3,4
                }
                console.log(i); // 4
            }
            console.log(i); // 25 (Because of functional scope)

    **let is block scope, so outside of loop or conditions we get Reference Error when we try to access to variable which is declared with let.EX:
            
            function checkLet(){
                for(let i=0;i<5;i++){
                    console.log(i); // 0,1,2,3,4
                }
                console.log(i); // Reference Error
            }
            let i=15;
            console.log(i); // 15

    **const -> most of specifities of const are the same as let. Difference is that const value is constant, it means that we can't change the value of const variable.Actually, it is used for that: give value to one variable which you are sure that it will never change during the coding.EX:
            
            const value=5;
            value=7; //Error

3.Hoisting -> JavaScript interpreter changes code such as:
            
            console.log(a) //undefined //From
            var a;

            var a;
            console.log(a);//undefined //To
So it is hoisting.
 */

        let visitWebsite= () =>{
            
            let input; // undefined
            while(input==undefined){ // When If condition(inside of function) is true input gets some value and loop is finished.
                
                const userName= prompt('Enter your name: ');
                const userAge= prompt('Enter your age: ');
                const isNoData= (userName==="")||(userName==undefined)||(userName==null)||(isNaN(userAge))||(userAge<0)||(userAge==="")||(userAge==null)||(userAge==undefined); // User's name shouldn't be empty or non-string. User's age should be greater than zero and shouldn't be non a number.

                if(!isNoData){ // If user enter meaningful datas, it despite isNoData boolean. So, we check input of user for !isNoData('Yes data?') 
                    
                    if(userAge<18){
                        alert('You are not allowed to visit this website.');
                    } else if(userAge>=18&&userAge<=22){ //works

                        let userChoice= confirm('Are you sure you want to continue?');
                        
                        if(userChoice==true){
                            alert(`Welcome, ${userName}`); //works
                        } else{
                            alert('You are not allowed to visit this website.'); //works
                        }
       
                    } else{
                            alert(`Welcome, ${userName}`); //works
                    }
                    input=userName; // If user input datas, then we don't need to continue the loop. So, we give value to input variable such as user's name or age it doesn't matter(important that it gets different value from null) for stopping its null.
                } 
                else{
                    alert('Datas are not meaningful. You are not allowed to visit this website.')
                }
            }       
        }
        visitWebsite(); // calling fuction.
















