/*
It can only be used on Arrays, Maps, and Sets.forEach will executed each element in array. For example;
    const arr = ['cat', 'dog', 'fish'];
    arr.forEach(element => {
        console.log(element);
    });
    // cat
    // dog
    // fish
*/

    let result = [];
    function filterBy(arr, dataType){
        arr.forEach(element=>{
            if(typeof element!==dataType){
                result.push(element);
            }
        })
        
        return result;
    }

console.log(filterBy([1,2,4,5,"lol"],'number'));
