/*
1.Functions is block-scope that are used for different purposes.Firstly, We see functions in some programming languages such as main() , static void Main(string[] args) and.etc. These are the main part of programming languages which all codes,functions are runned here and come with prog.languages.
Also we can create our own functions for avoiding repetitions.So, after created function we use this during coding by only call this.We can use this for different purposes.For instance; return a value.Also we create functions inside of functions
2.A parameter is like a placeholder. When a function is defined, you pass a value as a parameter. This value is referred to as the actual parameter or argument. The parameter list refers to the type, order, and number of the parameters of a function. Parameters are optional; that is why, a function may contain no parameters, It depends on us.For example;
    function sum(a=5,b=8){ // we give default values 5 and 8. That's why when we dont give value to a or b it calculate operation with default values
    return a+b;
}
    sum(3); //a=3,b=8
    sum(,7); //a=5,b=7
*/
function operation(){
    let input;
    while(input==undefined){
        const x=prompt('Enter number1:'); //I can't show default values, please explain or send me way of this after checking.
        const y=prompt('Enter number2:');
        const num1=parseFloat(x);
        const num2=parseFloat(y);
        //console.log(num1, num2)
        const operator=prompt('Enter operator:');
        const isNoData= (isNaN(num1))||(isNaN(num2));
        if(!isNoData){
            if(operator=='*'){
                console.log(`Result:${num1}${operator}${num2}=${num1*num2}`);
                input=num1;
            }else if(operator=='/'){
                console.log(`Result:${num1}${operator}${num2}=${num1/num2}`);
                input=num1;
            }else if(operator=='+'){
                console.log(`Result:${num1}${operator}${num2}=${num1+num2}`);
                input=num1;
            }else if(operator=='-'){
                console.log(`Result:${num1}${operator}${num2}=${num1-num2}`); // Only results are executed in console because of task condition.
                input=num1;
            }else{
                alert('Wrong operation!');
            }
        }
        else{
            alert("Wrong datas!");
        }
    }
}
operation();

