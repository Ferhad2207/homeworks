/*
    We can say that, most things in JS is object.   Variables can contain only single value,but objects values such as name:value and seperated by colon(,) with each other.
Methods are actions of objects which can be performed by functions or other methods.There are different ways to create objects and many properties for object such as comparing defining value again and etc.
But why we use objects and where? We can say everywhere and for different purposes. For example; we have person object and we should describe person with properties and what can he do(these are methods)
    let person={
        name:"Ferhad",//property
        surname:"Mehdizade",//pro
        age:"17",//pro
        job:"student",//pro
        swimming=()=>`${name} can swim`,//method
        driving=()=>`${name} cannot drive`//method
    }
    we can create hundreads of person with this object(easy-way) and changing values(deep-type cloning , not shallow type).These may be best when we create people class and create objects in this. 
*/

function CreateNewUser(){
    this.firstName=prompt('Enter your first name:');
    this.lastName=prompt('Enter your last name:');
    
    this.setFirstName=(name)=>{
        return this.firstName = name;    
    }

    this.setLastname = (surname)=>{
        return this.lastName = surname;
    }
    
    this.getLogin=()=>console.log(`${this.firstName[0]}${this.lastName}`.toLowerCase());

}

let user=new CreateNewUser();
user.getLogin();
user.setFirstName("james");
user.setLastname("bond");
user.getLogin();

