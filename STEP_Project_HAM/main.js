let section2Menubar = document.querySelectorAll('.section-2-menubar');
let section2Article = document.querySelectorAll('.section-2-article');
let sc4menubaroptions = document.querySelectorAll('.sc4-menubar-options');
let sc4ImgsArea = document.querySelectorAll('.section-4-imgs-area');
let sc4Btn = document.getElementById('section-4-btn');
let section4Images = document.getElementById('section-4-images');
let animation = document.querySelector('.container');
let section4ImgsAdd = document.querySelector('.section-4-imgs-area-add');

    for(let i=0;i<section2Menubar.length;i++){
        section2Menubar[i].addEventListener('click', () => {
            for(let j=0;j<section2Menubar.length;j++){
                section2Menubar[j].children[0].classList.remove('menubar-options-active');
                section2Menubar[j].children[1].classList.remove('menubar-triangles-active');
                section2Article[j].classList.remove('section-2-article-active');
            }
            section2Menubar[i].children[0].classList.add('menubar-options-active');
            section2Menubar[i].children[1].classList.add('menubar-triangles-active');
            section2Article[i].classList.add('section-2-article-active');
        })
    }

    sc4Btn.onclick = () => {
        sc4Btn.style.display = 'none';
        animation.style.display = 'flex'
        setTimeout(() => {
            section4ImgsAdd.style.display = 'flex';
            animation.style.display = 'none';
        }, 2300);
    }

    for(let i=0 ; i<sc4menubaroptions.length;i++){
        sc4menubaroptions[i].addEventListener('click', () => {
        for(let j=0 ; j<sc4menubaroptions.length;j++){
            sc4menubaroptions[j].classList.remove('sc4-menubar-options-active');
            sc4ImgsArea[j].classList.remove('section-4-imgs-area-active');
        }
        sc4menubaroptions[i].classList.add('sc4-menubar-options-active');
        sc4ImgsArea[i].classList.add('section-4-imgs-area-active');
        if(i==0){
            sc4Btn.style.display = 'flex';
            if(section4ImgsAdd.style.display == 'flex'){
                sc4Btn.style.display = 'none';
            }
        }else{
            sc4Btn.style.display = 'none';
            section4ImgsAdd.style.display = 'none';
        }
    })
    }